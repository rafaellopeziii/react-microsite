import ​{ override, fixBabelImports, addLessLoader } from 'customize-cra"';

module.exports = override(
    fixBabelImports("import", {
        libraryName: "antd",
        libraryDirectory: "es",
        style: true,
    }),
    addLessLoader({
        javascriptEnabled: true,
    })
);