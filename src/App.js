import React from 'react'
import './App.scss';
import MainHeader from './shared/Header'
import {Layout} from 'antd'
import Homepage from './modules/Homepage';
const { Header, Content } = Layout;
function App() {
  return (
    <div className="App">
      <Layout className="main-layout">
            <Header className="main-header" style={{ position: 'fixed', zIndex: 1, width: '100%' }}> 
              <MainHeader />
            </Header>
            <Content className="main-content" >
              <Homepage />
            </Content>
      </Layout>
    </div>
  );
}

export default App;
