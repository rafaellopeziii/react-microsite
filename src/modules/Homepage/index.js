import React from 'react'
import './index.scss'

import TcLogo from '../../assets/images/tclogo.png';
import CloudOne from '../../assets/images/1stcloud.png';
import CloudThree from '../../assets/images/2ndcloud.png';
import CloudTwo from '../../assets/images/3rdcloud.png';
import TcText1 from '../../assets/images/tctext.png';
import TcText2 from '../../assets/images/tctext2.png';
import Products from '../../assets/images/products.png';
import Powder from '../../assets/images/powder.png';
import Hearts from '../../assets/images/hearts.png';

import PageForm from '../../shared/PageForm';


const Homepage = () => {
    return (
        <div className="home-page">
            <div className="first-layout">
                <div className="left-clouds">
                    <img className='cloud-one' src={CloudOne} alt='cloud one'/>
                    <img className='cloud-two' src={CloudTwo} alt='cloud two'/>
                </div>
                <img className='tc-logo' src={TcLogo} alt='tclogo'/>
                <img className='cloud-three' src={CloudThree} alt='cloud three'/>
            </div>
            <div className="second-layout">
                
                <div className="text">
                    <img className='tc-text' src={TcText1} alt='tc-text'/>
                    <img className='tc-text2' src={TcText2} alt='tc-text2'/>
                </div>

                <div className="products">
                    <img className='product1'src={Products} alt='products'/>
                    <img className='powder'src={Powder} alt='powder'/>
                </div>
                <img className='hearts'src={Hearts} alt='powder'/>
            </div>
            
             <div className="third-layout">
                <PageForm />
             </div>
             

            
            
        </div>
            
        
    )
}

export default Homepage
