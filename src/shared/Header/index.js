import React from 'react'
import Logos from '../../assets/images/headerlogos.png'
import '../Header/index.scss'

const Header = () => {
    return (
        <header className="page-header">
            <div className="logos">
                <img src={Logos} alt="Logos" />
            </div>
            <div className="actions">
                <a href="/">Full Mechanics</a>
                <a href="/" className="join-promo-btn">Join Now</a>
            </div>
        </header>
    )
}

export default Header
