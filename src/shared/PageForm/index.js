import React from 'react';
import moment from 'moment';
import { Form, Input, Button, DatePicker,Select, message } from "antd";

import './index.scss';

const {Option} = Select;

const PageForm = () => {

    const [form] = Form.useForm();

    const onSubmit = async (values) => {
        console.log(`values`, values)
    }

    const onDate = () => {}

    const disabledDate = (current) => current && current < moment().endOf('day')

    const changeMercuryStoreBranch = (e) => {
        console.log(`e`, e)
    }

    const changeTenderCareProductType = (e) => {
        console.log(`e`, e)
    }
    const onPreventNumberOnly = (event) => { if (!/[0-9]/.test(event.key)) event.preventDefault()}
    
    return (
        
    
           <section className='form-container'>
            <h1>Join Now</h1>
           <Form
                form={form}
                name="form-join-now"
                // onFinish={onSubmit}
                hideRequiredMark
                scrollToFirstError
                layout="vertical"
            >   
                <Form.Item
                label='Mobile Number'
                name={["mobile_number"]}
                rules={[{ required: true, message: "Mobile Number is Required" }]}
                >
                <Input  onKeyPress={onPreventNumberOnly} placeholder='Mobile Number'/>
                </Form.Item>

                <Form.Item
                label='Mercury Store Branch'
                name={["mercury_store_branch"]}
                rules={[{ required: true, message: "Mercury Store Branch is Required" }]}
                >
                    <Select placeholder='Choose One' style={{ width:'100%' }} onChange={changeMercuryStoreBranch}>
                        <Option value="jack">Jack</Option>
                        <Option value="lucy">Lucy</Option>
                    </Select>
                </Form.Item>

                <Form.Item
                label='Tender Care Product Type'
                name={["tender_care_product_type"]}
                rules={[{ required: true, message: "Tender Care Product Type is Required" }]}
                >
                    <Select placeholder='Choose One' style={{ width:'100%' }} onChange={changeTenderCareProductType}>
                        <Option value="jack">Jack</Option>
                        <Option value="lucy">Lucy</Option>
                    </Select>
                </Form.Item>

                <Form.Item
                label='Amount Purchase (Minimum Php 120)'
                name={["amount_purchase"]}
                rules={[{ required: true, message: "Amount Purchase is Required" }]}
                >
                <Input placeholder='Total PHP'  onKeyPress={onPreventNumberOnly}/>
                </Form.Item>

          
                <Button className='btn-primary' htmlType="submit" block>Join Raffle</Button>
            </Form>


           </section>
     
    )
}

export default PageForm
